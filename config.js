module.exports = {
    platform: 'gitlab',
    baseBranches: ['master','feature1','main'],
    labels: ['renovate'],
    rebaseWhen: 'auto',
    semanticCommits: true,
    major: {
      automerge: false
    },
    repositories: [
      'shopizer'
    ],
    extends: [
      'config:base',
      ":pinAllExceptPeerDependencies"
    ]
  };
